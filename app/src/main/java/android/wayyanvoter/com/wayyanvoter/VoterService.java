package android.wayyanvoter.com.wayyanvoter;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Way Yan on 12/18/2015.
 */
public interface VoterService {
    @GET("/voters/{id}")
    Call<Voter>
    getPatient(@Path("id") int id);

    @POST("/voters")
    Call<Voter>
    savePatient(@Body Voter voter);

    @DELETE("/voters/{id}")
    Call<Voter>
    deletePatient(@Path("id") int id);
}
