package android.wayyanvoter.com.wayyanvoter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.wayyanvoter.com.wayyanvoter.Voter;
import android.wayyanvoter.com.wayyanvoter.VoterService;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

import retrofit.Call;
import retrofit.Callback;
import retrofit.JacksonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class NewAccountActivity extends AppCompatActivity {
    private EditText username;
    private EditText password;
    private Button finish;
    private Button cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_account);

        username = (EditText) findViewById(R.id.usernameField);
        password = (EditText) findViewById(R.id.passwordField);
        finish = (Button) findViewById(R.id.finish);
        cancel = (Button) findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewAccountActivity.this.finish();
            }
        });

        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (username.getText().toString().isEmpty()) {
                    username.setError("Username cannot be empty");
                }
                else if (password.getText().toString().isEmpty()) {
                    password.setError("Password cannot be empty");
                r
                else {
                    final OkHttpClient httpClient = new OkHttpClient();
                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl("http://10.0.2.2:8085")
                            .addConverterFactory(JacksonConverterFactory.create())
                            .client(httpClient)
                            .build();

                    VoterService voterService = retrofit.create(VoterService.class);
                    Voter voter = new Voter(username.getText().toString(), password.getText().toString());
                    Call<Voter> callPost = voterService.savePatient(voter);
                    callPost.enqueue(new Callback<Voter>() {
                        @Override
                        public void onResponse(Response<Voter> response, Retrofit retrofit) {
                            if (response.isSuccess()) {
                                Intent intent = new Intent(NewAccountActivity.this, MainActivity.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(NewAccountActivity.this, "Failed", Toast.LENGTH_SHORT).show();
                                Log.d("small fail", response.message());
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            Toast.makeText(NewAccountActivity.this, "Failed horribly", Toast.LENGTH_SHORT).show();
                            Log.d("big fail", throwable.getMessage());
                        }
                    });
                }
            }
        });
    }
}
