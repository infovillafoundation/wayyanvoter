package android.wayyanvoter.com.wayyanvoter;

/**
 * Created by Way Yan on 12/18/2015.
 */
public class Voter {
    private String username;
    private String password;

    public Voter() {

    }

    public Voter(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
